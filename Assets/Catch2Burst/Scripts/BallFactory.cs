using Catch2Burst.Scripts.Ball;
using UnityEngine;

namespace Catch2Burst.Scripts
{
    public class BallFactory : MonoBehaviour, IBallFactory
    {
        [SerializeField] private Ball.Ball _ballPrefab;

        public IBall Make(Color color, float speed, float award, float damage)
        {
            var ball = Instantiate(_ballPrefab, transform);

            ball.Speed = speed;
            ball.Award = award;
            ball.Damage = damage;
            
            ball.SetColor(color);

            return ball;
        }

        public void ReclaimAll()
        {
            foreach (Transform ballTransform in transform)
            {
                Destroy(ballTransform.gameObject);
            }
        }
    }
}
