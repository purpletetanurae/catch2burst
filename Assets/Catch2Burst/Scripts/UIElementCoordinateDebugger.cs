using System;
using UnityEngine;

namespace Catch2Burst.Scripts
{
    public class UIElementCoordinateDebugger : MonoBehaviour
    {
        [SerializeField] private Canvas _canvas;
        private RectTransform _rectTransform;

        private void Start()
        {
            var rectTransform = GetComponent<RectTransform>();
            _rectTransform = rectTransform;
            Debug.Log($"position: {transform.position}");
            Debug.Log($"rect center position: {rectTransform.rect.center}");
            Debug.Log($"rect position: {rectTransform.position}");
            Debug.Log($"rect anchored position: {rectTransform.anchoredPosition}");
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("Click");
                Debug.Log($"Mouse position: {Input.mousePosition}");
                Debug.Log($"screen to world point: {Camera.main.ScreenToWorldPoint(Input.mousePosition)}");
                if (RectTransformUtility.ScreenPointToWorldPointInRectangle(_rectTransform, Input.mousePosition, Camera.main, out Vector3 worldPoint))
                {
                    Debug.Log($"World point in rect: {worldPoint}");
                }
                if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_rectTransform, Input.mousePosition, Camera.main, out Vector2 localPoint))
                {
                    Debug.Log($"Local point in rect: {localPoint}");
                }
            }
        }
    }
}
