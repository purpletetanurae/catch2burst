using System;
using Catch2Burst.Scripts.Flying_Inscription;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Catch2Burst.Scripts
{
    public class Clicker : MonoBehaviour
    {
        [SerializeField] private FlyingInscriptionFactory _flyingInscriptionFactory;
        [SerializeField] private RectTransform _targetRectTransform;
        [SerializeField] private Canvas _canvas;

        private Camera _camera;

        private void Awake() => _camera = Camera.main;

        private float _score = 0f;

        private void Update()
        {
            if (!Input.GetMouseButtonDown(0)) return;

            _score += 1f;
            
            var flyingInscription = _flyingInscriptionFactory.Make(_score, Random.ColorHSV(), GetMouseWorldPoint());
                
            flyingInscription.FlyTo(_targetRectTransform.position, () => _flyingInscriptionFactory.Reclaim(flyingInscription));
        }

        private Vector3 GetMouseWorldPoint() =>
            !RectTransformUtility.ScreenPointToWorldPointInRectangle(_targetRectTransform,
                Input.mousePosition,
                _camera,
                out Vector3 worldPoint)
                ? Vector3.zero
                : worldPoint;
    }
}
