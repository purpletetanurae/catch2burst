using System;
using UnityEngine;

namespace Catch2Burst.Scripts.UI
{
    [RequireComponent(typeof(UIMediator))]
    public class GameObserver : MonoBehaviour
    {
        [SerializeField] private Game.Game _game;
        
        private UIMediator _mediator;

        private void Awake() => _mediator = GetComponent<UIMediator>();

        private void Start()
        {
            _mediator.ChangeHealth(_game.Health);
            _mediator.ChangeScore(_game.Score);
        }

        private void OnEnable()
        {
            _game.ScoreChanged += OnScoreChanged;
            _game.HealthChanged += OnHealthChanged;
            _game.Defeated += OnPlayerDefeated;
        }

        private void OnDisable()
        {
            _game.ScoreChanged -= OnScoreChanged;
            _game.HealthChanged -= OnHealthChanged;
            _game.Defeated -= OnPlayerDefeated;
        }

        private void OnPlayerDefeated()
        {
            _mediator.ShowDefeatMenu();
            _mediator.HidePauseButton();
            _mediator.HideGameUI();

            _mediator.ShowDefeatMenuScore(_game.Score, _game.BestScore);
        }

        public void LaunchGame()
        {
            _mediator.HideStartMenu();
            _mediator.ShowPauseButton();
            _mediator.ShowGameUI();

            _game.Launch();
        }

        public void PauseGame()
        {
            _mediator.HideGameUI();
            _mediator.HidePauseButton();
            _mediator.ShowPauseMenu();

            _game.Pause();
        }

        public void ResumeGame()
        {
            _mediator.ShowGameUI();
            _mediator.ShowPauseButton();
            _mediator.HidePauseMenu();

            _game.Resume();
        }

        public void RestartGame()
        {
            _mediator.HideDefeatMenu();
            _mediator.HidePauseMenu();
            _mediator.ShowGameUI();
            _mediator.ShowPauseButton();

            _game.Restart();
        }

        private void OnHealthChanged(float health) => _mediator.ChangeHealth(health);
        private void OnScoreChanged(float score) => _mediator.ChangeScore(score);
    }
}
