using TMPro;
using UnityEngine;

namespace Catch2Burst.Scripts.UI
{
    [RequireComponent(typeof(GameObserver))]
    public class UIMediator : MonoBehaviour
    {
        [SerializeField] private GameObject _startMenu;
        [SerializeField] private GameObject _pauseButton;
        [SerializeField] private GameObject _pauseMenu;
        [SerializeField] private GameObject _gameUI;
        [SerializeField] private GameObject _defeatMenu;

        [SerializeField] private TMP_Text _scoreField;
        [SerializeField] private TMP_Text _healthField;

        [SerializeField] private DefeatMenuScoreDisplayer _defeatMenuScoreDisplayer;

        private GameObserver _gameObserver;

        private void Awake()
        {
            _gameObserver = GetComponent<GameObserver>();
        }

        public void ShowPauseButton() => _pauseButton.SetActive(true);
        public void HidePauseButton() => _pauseButton.SetActive(false);

        public void ShowPauseMenu() => _pauseMenu.SetActive(true);
        public void HidePauseMenu() => _pauseMenu.SetActive(false);

        public void ShowGameUI() => _gameUI.SetActive(true);
        public void HideGameUI() => _gameUI.SetActive(false);

        public void ShowDefeatMenu() => _defeatMenu.SetActive(true);
        public void HideDefeatMenu() => _defeatMenu.SetActive(false);
        public void ShowDefeatMenuScore(float score, float bestScore) => _defeatMenuScoreDisplayer.ShowScore(score, bestScore);

        public void HideStartMenu() => _startMenu.SetActive(false);

        public void ChangeScore(float score) => _scoreField.text = score.ToString("F2");
        public void ChangeHealth(float health) => _healthField.text = health.ToString("F2");

        public void LaunchGame() => _gameObserver.LaunchGame();
        public void PauseGame() => _gameObserver.PauseGame();
        public void RestartGame() => _gameObserver.RestartGame();
        public void ResumeGame() => _gameObserver.ResumeGame();
    }
}
