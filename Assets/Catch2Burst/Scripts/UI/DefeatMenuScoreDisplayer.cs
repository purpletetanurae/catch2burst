using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DefeatMenuScoreDisplayer : MonoBehaviour
{
    [SerializeField] private TMP_Text _scoreField;
    [SerializeField] private TMP_Text _bestScoreField;

    public void ShowScore(float score, float bestScore)
    {
        _scoreField.text = score.ToString("F2");
        _bestScoreField.text = bestScore.ToString("F2");
    }
}
