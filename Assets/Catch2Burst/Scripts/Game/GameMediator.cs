using System;
using System.Collections.Generic;
using Catch2Burst.Scripts.Ball;
using UnityEngine;

namespace Catch2Burst.Scripts.Game
{
    [RequireComponent(typeof(BallSpawner))]
    [RequireComponent(typeof(Game))]
    [RequireComponent(typeof(GameTimer))]
    public class GameMediator : MonoBehaviour
    {
        private BallSpawner _ballSpawner;
        private Game _game;
        private GameTimer _timer;

        private void Awake()
        {
            _ballSpawner = GetComponent<BallSpawner>();
            _game = GetComponent<Game>();
            _timer = GetComponent<GameTimer>();
        }

        public void SetBallsSpeed(float speed) => _ballSpawner.BallsSpeed = speed;
        public void SetBallDamage(float damage) => _ballSpawner.BallsDamage = damage;
        public void SetBallsAward(float award) => _ballSpawner.BallsAward = award;
        public void SetBallsColors(IEnumerable<Color> colors) => _ballSpawner.BallsColors = colors;
        
        public void LaunchBall(IBall ball) => _game.LaunchBall(ball);

        public void LaunchTimer(float period) => _timer.Launch(period);
        public void PauseTimer() => _timer.Pause();
        public void ResumeTimer() => _timer.Resume();

        public void LaunchSpawner() => _ballSpawner.Launch();
        public void PauseSpawner() => _ballSpawner.Pause();
        public void ResumeSpawner() => _ballSpawner.Resume();

        public void IncreaseComplexity() => _game.IncreaseComplexity();
        public void DestroyAllBalls() => _ballSpawner.DestroyAllBalls();

        internal void SetSpawnPeriod(float spawnPeriod) => _ballSpawner.SpawnPeriod = spawnPeriod;
    }
}
