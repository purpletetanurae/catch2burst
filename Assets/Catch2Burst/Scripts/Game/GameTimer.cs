using System;
using System.Collections;
using Catch2Burst.Scripts.Ball;
using UnityEngine;

namespace Catch2Burst.Scripts.Game
{
    [RequireComponent(typeof(GameMediator))]
    public class GameTimer : MonoBehaviour
    {
        private GameMediator _mediator;
        private Coroutine _timerCoroutine;

        private float _period;

        private void Awake() => _mediator = GetComponent<GameMediator>();

        public void Launch(float period)
        {
            _period = period;
            _timerCoroutine = StartCoroutine(Timer(period));
        }

        public void Stop() => StopCoroutine(_timerCoroutine);
        public void Pause() => Stop();
        public void Resume() => _timerCoroutine = StartCoroutine(Timer(_period));

        private IEnumerator Timer(float period)
        {
            while (true)
            {
                yield return new WaitForSeconds(period);
                _mediator.IncreaseComplexity();
            }
        }
    }
}
