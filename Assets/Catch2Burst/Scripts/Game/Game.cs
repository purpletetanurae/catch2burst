using System;
using System.Collections.Generic;
using Catch2Burst.Scripts.Ball;
using Catch2Burst.Scripts.Scriptable_Objects;
using UnityEngine;

namespace Catch2Burst.Scripts.Game
{
    [RequireComponent(typeof(GameMediator))]
    public class Game : MonoBehaviour
    {
        public event Action<float> HealthChanged;
        public event Action<float> ScoreChanged;
        public event Action Defeated;

        public float BestScore { get; private set; }

        public float Health
        {
            get => _health;
            private set
            {
                _health = value;
                HealthChanged?.Invoke(_health);
            }
        }

        public float Score
        {
            get => _score;
            private set
            {
                _score = value;
                ScoreChanged?.Invoke(_score);
            }
        }
        
        [SerializeField] private GameConfiguration _configuration;
        
        private GameMediator _mediator;

        private int _complexity;
        private const int MaxComplexity = 100;
        private ValueCalculator _speedCalculator;
        private ValueCalculator _awardCalculator;
        private float _score;
        private float _health;
        
        private HashSet<IBall> _launchedBalls = new HashSet<IBall>();

        private void Awake()
        {
            _mediator = GetComponent<GameMediator>();

            _speedCalculator = new ValueCalculator(_configuration.StartSpeed, _configuration.MaxSpeed, MaxComplexity);
            _awardCalculator = new ValueCalculator(_configuration.Award, _configuration.MaxAward, MaxComplexity);
        }

        public void Launch()
        {
            _complexity = 0;

            Score = 0;
            Health = _configuration.PlayerHealth;

            _mediator.SetBallsSpeed(_speedCalculator.Calculate(_complexity));
            _mediator.SetBallsAward(_awardCalculator.Calculate(_complexity));
            _mediator.SetBallDamage(_configuration.Damage);
            _mediator.SetBallsColors(_configuration.BallColors);
            _mediator.SetSpawnPeriod(_configuration.SpawnPeriod);

            _mediator.LaunchSpawner();
            _mediator.LaunchTimer(_configuration.ComplexityIncreasingPeriod);
        }

        public void Pause()
        {
            _mediator.PauseSpawner();
            _mediator.PauseTimer();

            foreach (var ball in _launchedBalls)
            {
                ball.Pause();
            }
        }

        public void Resume()
        {
            _mediator.ResumeSpawner();
            _mediator.ResumeTimer();

            foreach (var ball in _launchedBalls)
            {
                ball.Resume();
            }
        }

        public void Restart()
        {
            _launchedBalls.Clear();
            _mediator.DestroyAllBalls();
            Launch();
        }

        public void IncreaseComplexity()
        {
            _complexity++;
            _mediator.SetBallsSpeed(_speedCalculator.Calculate(_complexity));
            _mediator.SetBallsAward(_awardCalculator.Calculate(_complexity));
        }

        public void LaunchBall(IBall ball)
        {
            ball.Launch(OnBallAwarded, OnBallDamaged);
            _launchedBalls.Add(ball);
        }

        private void OnBallAwarded(IBall ball, float award)
        {
            Score += award;
            _launchedBalls.Remove(ball);
        }

        private void OnBallDamaged(IBall ball, float damage)
        {
            Health -= damage;
            _launchedBalls.Remove(ball);

            CheckDefeat();
        }

        private void CheckDefeat()
        {
            if (Health > 0)
            {
                return;
            }

            Pause();
            UpdateBestScore();
            Defeated?.Invoke();
        }

        private void UpdateBestScore()
        {
            if (_score > BestScore)
            {
                BestScore = _score;
            }
        }
    }
}
