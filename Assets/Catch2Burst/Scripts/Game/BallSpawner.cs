using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Catch2Burst.Scripts.Ball;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Catch2Burst.Scripts.Game
{
    [RequireComponent(typeof(GameMediator))]
    public class BallSpawner : MonoBehaviour
    {
        [SerializeField] private BallFactory _ballFactory;

        [SerializeField] private Transform[] _spawnPoints = new Transform[2];

        private Coroutine _spawningCoroutine;
        private GameMediator _mediator;
        
        public float BallsSpeed { private get; set; }
        public float BallsAward { private get; set; }
        public float BallsDamage { private get; set; }
        public IEnumerable<Color> BallsColors { private get; set; }
        public float SpawnPeriod { get; set; }

        private void Awake() => _mediator = GetComponent<GameMediator>();
        public void Launch() => _spawningCoroutine = StartCoroutine(Spawning());
        public void Pause() => Stop();
        public void Resume() => Launch();
        public void Stop() => StopCoroutine(_spawningCoroutine);
        public void DestroyAllBalls() => _ballFactory.ReclaimAll();

        private IEnumerator Spawning()
        {
            while (true)
            {
                yield return new WaitForSeconds(SpawnPeriod);
                Spawn();
            }
        }

        private void Spawn()
        {
            var ball = CreateBall();
            ball.Position = CalculateRandomSpawnPosition();
            
            _mediator.LaunchBall(ball);
        }

        private Vector3 CalculateRandomSpawnPosition()
        {
            var spawnPointsCount = _spawnPoints.Length;

            if (spawnPointsCount == 0)
            {
                return Vector3.zero;
            }

            if (spawnPointsCount == 1)
            {
                return _spawnPoints[0].position;
            }

            var y = Mathf.Min(_spawnPoints[0].position.y, _spawnPoints[1].position.y);
            var x = Random.Range(_spawnPoints[0].position.x, _spawnPoints[1].position.x);

            return new Vector3(x, y, _spawnPoints[0].position.z);
        }

        private IBall CreateBall()
        {
            return _ballFactory.Make(GetRandomColor(), BallsSpeed, BallsAward, BallsDamage);
        }

        private Color GetRandomColor()
        {
            var colorCount = BallsColors.Count();
            var randomIndex = Random.Range(0, colorCount);

            return BallsColors.ToArray()[randomIndex];
        }
    }
}
