using UnityEngine;

namespace Catch2Burst.Scripts.Game
{
    public class ValueCalculator
    {
        private readonly float _min;
        private readonly float _max;
        private readonly int _maxComplexity;

        public ValueCalculator(float min, float max, int maxComplexity)
        {
            _min = min;
            _max = max;
            _maxComplexity = maxComplexity;
        }

        public float Calculate(int complexity)
        {
            var t = Mathf.InverseLerp(0, _maxComplexity, complexity);
            return Mathf.Lerp(_min, _max, t);
        }
    }
}
