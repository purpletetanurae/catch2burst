﻿using Catch2Burst.Scripts.Ball;
using UnityEngine;

namespace Catch2Burst.Scripts
{
    public interface IBallFactory
    {
        IBall Make(Color color, float speed, float award, float damage);
    }
}