using System.Collections.Generic;
using UnityEngine;

namespace Catch2Burst.Scripts.Scriptable_Objects
{
    [CreateAssetMenu(menuName = "Catch2Burst/Ball Colors", fileName = "Ball Colors")]
    public class BallColors : ScriptableObject
    {
        public IEnumerable<Color> Colors => _colors;

        [SerializeField] private List<Color> _colors;
    }
}
