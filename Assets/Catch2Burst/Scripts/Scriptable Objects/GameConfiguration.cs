using System.Collections.Generic;
using UnityEngine;

namespace Catch2Burst.Scripts.Scriptable_Objects
{
    [CreateAssetMenu(menuName = "Catch2Burst/Game Configuration", fileName = "Game Configuration")]
    public class GameConfiguration : ScriptableObject
    {
        public float StartSpeed => _startSpeed;
        public float MaxSpeed => _maxSpeed;
        public float ComplexityIncreasingPeriod => _complexityIncreasingPeriod;
        public float Award => _award;
        public float Damage => _damage;
        public float PlayerHealth => _playerHealth;
        public IEnumerable<Color> BallColors => _ballColors.Colors;
        public float MaxAward => _award * 10;
        public float SpawnPeriod => _spawnPeriod;

        [SerializeField, Min(0)] private float _startSpeed;
        [SerializeField, Min(0)] private float _maxSpeed;
        [SerializeField, Min(0)] private float _complexityIncreasingPeriod;
        [SerializeField, Min(0)] private float _speedIncreasingStep;

        [SerializeField, Min(0)] private float _award;
        [SerializeField, Min(0)] private float _damage;

        [SerializeField, Min(0)] private float _playerHealth;

        [SerializeField] private BallColors _ballColors;
        [SerializeField, Min(0)] private float _spawnPeriod;
    }
}
