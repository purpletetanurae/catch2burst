using System;
using UnityEngine;

namespace Catch2Burst.Scripts.Ball
{
    [RequireComponent(typeof(ParticleSystem))]
    public class Particles : MonoBehaviour
    {
        public event Action Stopped;
        
        private ParticleSystem _particleSystem;

        private void Awake() => _particleSystem = GetComponent<ParticleSystem>();

        public void SetColor(Color color)
        {
            var particleSystemMain = _particleSystem.main;
            particleSystemMain.startColor = color;
        }

        public void Play() => _particleSystem.Play();

        private void OnParticleSystemStopped() => Stopped?.Invoke();
    }
}
