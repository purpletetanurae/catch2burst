using UnityEngine;
using UnityEngine.EventSystems;

namespace Catch2Burst.Scripts.Ball
{
    [RequireComponent(typeof(BallMediator))]
    public class BallClickDetector : MonoBehaviour, IPointerClickHandler
    {
        private BallMediator _mediator;

        private void Awake() => _mediator = GetComponent<BallMediator>();

        public void OnPointerClick(PointerEventData eventData)
        {
            _mediator.HandleBallClick();
        }
    }
}
