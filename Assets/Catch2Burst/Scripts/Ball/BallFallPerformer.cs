using UnityEngine;

namespace Catch2Burst.Scripts.Ball
{
    public class BallFallPerformer : MonoBehaviour
    {
        public float Speed
        {
            set => _speed = value;
        }

        [SerializeField] private float _speed;
        
        private Transform _transform;

        private void Awake() => _transform = transform;

        private void Update()
        {
            if (IsStopped) return;

            Fall();
        }

        private bool IsStopped => _speed == 0;

        private void Fall()
        {
            _transform.position += Vector3.down * _speed * Time.deltaTime;
        }
    }
}
