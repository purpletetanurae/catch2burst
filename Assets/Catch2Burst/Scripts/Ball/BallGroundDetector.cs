using UnityEngine;

namespace Catch2Burst.Scripts.Ball
{
    [RequireComponent(typeof(BallMediator))]
    [RequireComponent(typeof(Collider))]
    public class BallGroundDetector : MonoBehaviour
    {
        [SerializeField] private string _groundTag; 
            
        private BallMediator _mediator;

        private void Awake() => _mediator = GetComponent<BallMediator>();

        private void OnTriggerEnter(Collider other)
        {
            if (!IsGround(other)) return;

            _mediator.HandleGroundDetection();
        }

        private bool IsGround(Component other) => other.tag.Equals(_groundTag);
    }
}
