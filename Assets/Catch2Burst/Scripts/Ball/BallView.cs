using UnityEngine;

namespace Catch2Burst.Scripts.Ball
{
    [RequireComponent(typeof(MeshRenderer))]
    public class BallView : MonoBehaviour
    {
        private Material _material;

        private void Awake()
        {
            _material = GetComponent<MeshRenderer>().material;
        }

        public void SetColor(Color color)
        {
            _material.color = color;
        }
    }
}
