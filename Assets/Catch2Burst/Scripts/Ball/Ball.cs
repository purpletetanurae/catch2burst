using System;
using UnityEngine;

namespace Catch2Burst.Scripts.Ball
{
    [RequireComponent(typeof(BallMediator))]
    public class Ball : MonoBehaviour, IBall
    {
        private BallMediator _mediator;

        public float Award { get; set; }
        public float Damage { get; set; }
        public float Speed { get; set; }

        private Action<IBall, float> _awardHandler;
        private Action<IBall, float> _damageApplyHandler;

        private void Awake() => _mediator = GetComponent<BallMediator>();

        public void SetColor(Color color)
        {
            _mediator.SetBodyColor(color);
            _mediator.SetParticlesColor(color);
        }

        public Vector3 Position
        {
            get => transform.position;
            set => transform.position = value;
        }

        public void Launch(Action<IBall, float> awardHandler, Action<IBall, float> damageApplyHandler)
        {
            _awardHandler = awardHandler;
            _damageApplyHandler = damageApplyHandler;
            
            _mediator.SetSpeed(Speed);
        }

        public void Pause()
        {
            _mediator.ToggleInteraction(false);
            _mediator.SetSpeed(0);
        }

        public void Resume()
        {
            _mediator.ToggleInteraction(true);
            _mediator.SetSpeed(Speed);
        }

        private void PerformAward() => _awardHandler.Invoke(this, Award);

        private void DealDamage() => _damageApplyHandler.Invoke(this, Damage);

        public void HandleGroundDetection()
        {
            DealDamage();
            PerformBurst();
        }

        public void HandleBallClick()
        {
            PerformAward();
            PerformBurst();
        }

        private void PerformBurst()
        {
            _mediator.ToggleInteraction(false);
            _mediator.SetSpeed(0);
            _mediator.Burst();
        }

        public void Destroy() => Destroy(gameObject);
    }
}
