using System;
using UnityEngine;

namespace Catch2Burst.Scripts.Ball
{
    public interface IBall
    {
        Vector3 Position { get; set; }
        void Launch(Action<IBall, float> awardHandler, Action<IBall, float> damageApplyHandler);
        void Pause();
        void Resume();
    }
}
