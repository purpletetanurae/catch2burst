using UnityEngine;

namespace Catch2Burst.Scripts.Ball
{
    [RequireComponent(typeof(BallClickDetector))]
    [RequireComponent(typeof(BallFallPerformer))]
    [RequireComponent(typeof(BallAnimator))]
    [RequireComponent(typeof(Ball))]
    [RequireComponent(typeof(BallGroundDetector))]
    [RequireComponent(typeof(Collider))]
    public class BallMediator : MonoBehaviour
    {
        [SerializeField] private BallView _ballView;
        [SerializeField] private Particles _particles;
        
        private BallFallPerformer _fallPerformer;
        private BallAnimator _ballAnimator;
        private Ball _ball;
        private Collider _collider;

        private void Awake()
        {
            _fallPerformer = GetComponent<BallFallPerformer>();
            _ballAnimator = GetComponent<BallAnimator>();
            _ball = GetComponent<Ball>();
            _collider = GetComponent<Collider>();
        }

        public void SetSpeed(float speed) => _fallPerformer.Speed = speed;
        public void Burst() => _ballAnimator.AnimateBurst();
        public void SetBodyColor(Color color) => _ballView.SetColor(color);
        public void SetParticlesColor(Color color) => _particles.SetColor(color);
        public void HandleGroundDetection() => _ball.HandleGroundDetection();
        public void HandleBallClick() => _ball.HandleBallClick();
        public void ToggleInteraction(bool isInteractable) => _collider.enabled = isInteractable;
        public void HandleBurstFinished() => _ball.Destroy();
    }
}
