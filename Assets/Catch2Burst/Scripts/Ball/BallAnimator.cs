using DG.Tweening;
using UnityEngine;

namespace Catch2Burst.Scripts.Ball
{
    [RequireComponent(typeof(BallMediator))]
    public class BallAnimator : MonoBehaviour
    {
        [SerializeField] private Transform _viewTransform;
        [SerializeField] private Particles _particles;

        private BallMediator _mediator;
        private const float TransformDuration = 0.5f;
        private Sequence _sequence;

        public void AnimateBurst()
        {
            _sequence = DOTween.Sequence();

            _sequence.Append(_viewTransform.DOScale(Vector3.zero, TransformDuration).SetEase(Ease.InBack));
            _sequence.AppendCallback(_particles.Play);
        }

        private void Awake() => _mediator = GetComponent<BallMediator>();

        private void OnEnable() => _particles.Stopped += OnParticlesStopped;

        private void OnDisable() => _particles.Stopped -= OnParticlesStopped;

        private void OnParticlesStopped()
        {
            _mediator.HandleBurstFinished();
        }
    }
}
