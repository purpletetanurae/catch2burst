using UnityEngine;

namespace Catch2Burst.Scripts.Flying_Inscription
{
    public class FlyingInscriptionFactory : MonoBehaviour, IFlyingInscriptionFactory
    {
        [SerializeField] private FlyingInscription _flyingInscriptionPrefab;

        public FlyingInscription Make(float value, Color color, Vector3 position)
        {
            var flyingInscription = Instantiate(_flyingInscriptionPrefab, transform);

            flyingInscription.Position = position;
            flyingInscription.Text = value.ToString();
            flyingInscription.Color = color;

            return flyingInscription;
        }

        public void Reclaim(FlyingInscription flyingInscription)
        {
            Destroy(flyingInscription.gameObject);
        }
    }
}
