using System;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Catch2Burst.Scripts.Flying_Inscription
{
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(TMP_Text))]
    public class FlyingInscription : MonoBehaviour
    {
        public Color Color
        {
            set => _textField.color = value;
        }

        public string Text
        {
            set => _textField.text = value;
        }

        public Vector3 Position
        {
            set => _rectTransform.position = value;
        }
        
        private RectTransform _rectTransform;
        private TMP_Text _textField;

        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
            _textField = GetComponent<TMP_Text>();
        }

        public void FlyTo(Vector3 position, Action finished)
        {
            var sequence = DOTween.Sequence();
            sequence.Append(_rectTransform.DOMove(position, 1f).SetEase(Ease.InBack));
            sequence.AppendCallback(finished.Invoke);
        }
    }
}
