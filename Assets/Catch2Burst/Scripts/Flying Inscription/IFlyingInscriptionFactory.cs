﻿using UnityEngine;

namespace Catch2Burst.Scripts.Flying_Inscription
{
    public interface IFlyingInscriptionFactory
    {
        FlyingInscription Make(float value, Color color, Vector3 position);
        void Reclaim(FlyingInscription flyingInscription);
    }
}